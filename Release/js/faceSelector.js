/*
Face Selector is the object that handles clicking on a face and the clicking on a 
video to select that video as the 'liked' or 'disliked' video.
*/

var util = require("./util.js");//dependency

var FaceSelector = function (thumbnails) {
    this.happyButton = document.getElementById("happy");//happy button 
    this.sadButton = document.getElementById("sad");//sad button
    this.selectedFace = null;
    this.thumbnails = thumbnails;//the list of thumbnails for the video set.

    this.redrawThumbnails = function () {//when selected redraw them so they can be 'de-selected'
        for (var pos = 0; pos < util.numThumbnails; pos++) {
            this.thumbnails[pos].drawImage();//draw over the thumbnails
        }
    };

    this.selectHappy = function () {//when the happy face is selected: change styles
        this.selectedFace = "happy";
        this.happyButton.style.outlineStyle = "solid";
        this.happyButton.style.outlineColor = "#00ff00";
        this.sadButton.style.outlineStyle = "none";
        this.redrawThumbnails();
    };

    this.selectSad = function () {//when sad face is selected change styles
        this.selectedFace = "sad";
        this.sadButton.style.outlineStyle = "solid";
        this.sadButton.style.outlineColor = "#ff0000";
        this.happyButton.style.outlineStyle = "none";
        this.redrawThumbnails();
    };

    this.deselectFace = function () {//undo the style changes when deselected
        this.selectedFace = null;
        this.happyButton.style.outlineStyle = "none";
        this.sadButton.style.outlineStyle = "none";
        this.redrawThumbnails();
    };

    var self = this;//reference to this object
    this.happyClickHandler = function () {//set the click-selected status
        if (self.selectedFace == "happy") {
            self.deselectFace();
        } else {
            self.selectHappy();
        }
    };

    this.sadClickHandler = function () {//set the click-selected status
        if (self.selectedFace == "sad") {
            self.deselectFace();
        } else {
            self.selectSad();
        }
    };
	
	//make the things happen on click event
    this.happyButton.addEventListener("click", this.happyClickHandler);//add to eventlistner
    this.sadButton.addEventListener("click", this.sadClickHandler);
};

module.exports.FaceSelector = FaceSelector;
