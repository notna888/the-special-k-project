window.onload = function () {
    //var results = [-0.2, 0, 0.2, 0.4, 0.8, 0, -0.2, -0.2, 0.4000000000000001, -0.2, 0, -0.2, -0.4000000000000001, -0.2, 0.2, -0.4000000000000001, 0.4, -0.2, -0.6000000000000001, 0, 0.4];
    // ^ are some values I got from running through the test once, treating it as a real world example in case of testing leaving it here maybe?

    //make the line bellow a comment to make result page work on it's own - remove this up to and including line 2 when project is complete 
	//loads the results from the test from the sessionStorage
	var results;
		if(typeof sessionStorage.result === "undefined") {
			alert("There was an error with your results");
			results = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21];
		} else
    		results = JSON.parse(sessionStorage.result);
    var resultObject = [{index:"", value:""}];//a list and value of each vid
	
	//sets the resultObject with data
    for(i = 0; i < results.length; i++){
	    resultObject[i] = {index: i , value: results[i]};
	};
    resultObject.sort(function (a, b) {
        return b.value - a.value//sorts it highest to lowest
    });

    var resultImages = [];//list of result images
    for(i = 0; i < results.length; i++){
	    resultImages[i] = resultObject[i].index + 1;//load each image index in 
    };
    
	//get the top and bottom 3 for viewing and displaying to the html
    var top = resultImages.slice(0,3);
    console.log(top);
    var bot = resultImages.slice(18,21);
    console.log(bot);

    var resultsToDraw = top.concat(bot);
    
//---------------------This bit bellow does the actual drawing----------------------\\


    //6 divs named exactly those numbers
    var divToDraw = ["1","2","3","19","20","21"]; //The numbers of the divs we are drawing into on the html, could've been just 1 to 6 but this makes it clearer what's happening
	//pushes html image elements into the html. 
    for (var i = 0; i < 6; i++){
        var y = resultsToDraw[i];
	    var img = i+1
	    var x = '<img id= "image' + img  +  '" src = "assets/thumbnails/thumbnail' + y + '.jpg">';
	    var thisDiv = "result" + divToDraw[i];
	    document.getElementById(thisDiv).innerHTML += x;
    };       
	
    var PAGE_URL = "http://localhost/?page=result_post";

	document.getElementById("submit").onclick = function () {

		// Show "sending" bar
		// sending.show(); //?
		$.ajax(PAGE_URL, {
			data: {
				res_id: 2,
				res_hash: 1234,
				question: 2,
				best: 1,
				worst: 2
			},
			success: resultOnSuccess,
			error: resultOnError
		});
	};

	var resultOnSuccess = function(data) {

	}

	var resultOnError = function(xhr, status, errorType) {

	}
}

