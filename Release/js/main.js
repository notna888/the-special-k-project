/*
Manages and starts the entire process:
Creates each neeeded element and their dependencies once window has loaded
*/
var util = require("./util.js");
var ajaxLoading = require("./pace.min.js");

//saves and creates each element necessary for the apps running
var VideoSelector = require("./videoSelector.js").VideoSelector;
var FaceSelector = require("./faceSelector.js").FaceSelector;
var Video = require("./video.js").Video;
var Thumbnail = require("./thumbnail.js").Thumbnail;
var Results = require("./results.js").Results;
var loadingBar = require("./loadingBar.js").progress;

//attach most of the operations to only run once the window has finished loading
if (window.attachEvent) {
    window.attachEvent('onload', start);
} else if(window.onload) {
    var currentOnload = window.onload;
    window.onload = function() {
        currentOnload();
        start();
    };
} else {
    window.onload = start;
}

//called once window has finished loading all elements
function start() {
    //check some basic things:
    if (typeof (Storage) !== "undefined") {
        // Code for localStorage/sessionStorage.
    } else {
        // Sorry! No Web Storage support..
        console.log("No webstorage; Website may not function as expected.");
        alert("No webstorage; Website may not function as expected.");
    }


    // Build data structures
    //first start the automatic progress bar on ajax loading elements
    ajaxLoading.start();

    //then the progress bar loading stuff
    var lB = new loadingBar();
    lB.draw();

	//arrays to hold designated items
    var videos = [];
    var thumbnails = [];
    var results = new Results();
    var videoSelector = new VideoSelector(videos, thumbnails, results, lB);//video selection
    var faceSelector = new FaceSelector(thumbnails);//happy/sasd (like/dislike) operations

    // Populate data structures
    for (var vidID = 0; vidID < util.numVideos; vidID++) {
        videos.push(new Video(vidID));
    }
    for (var pos = 0; pos < util.numThumbnails; pos++) {
        thumbnails.push(new Thumbnail(pos));
        thumbnails[pos].videoSelector = videoSelector;
        thumbnails[pos].faceSelector = faceSelector;
        thumbnails[pos].results = results;
    }
    results.videoSelector = videoSelector;

    // Get first video and thumbnails
    videoSelector.loadVideo();
    videoSelector.loadThumbnails();

    
};

//mouse button pressing to play or pause a video on click
function playpause() {
    console.log('things');
    var vid = document.getElementById('vidbox');
    if (vid.paused)
        vid.play();
    else
        vid.pause();
    end
}
