/*
Object handles the thumbnail selection, drawing, and highlighting.
*/

var util = require("./util.js");

var Thumbnail = function (pos) {
    this.pos = pos;//position in the set
    this.canvas = document.getElementById("thumb" + (pos+1) + "c");//that particular elements 
    this.context = this.canvas.getContext("2d");//the 2d context of given canvas

    this.videoSelector = null;
    this.faceSelector = null;
    this.results = null;
    this.video = null;
    this.happyOver = new Image();
    this.happyOver.src = "./assets/images/happyover.png";
    this.sadOver = new Image();
    this.sadOver.src = "./assets/images/sadover.png";
	
	//changes a video over
    var self = this;
    this.setVideo = function (video) {
        if (this.video != null) {
            this.video.thumbnail = null;
        }
        this.video = video;
        this.video.thumbnail = this;
        this.drawImage();
    };
	
	//draws the thumbnails depending on their state (highlighted, etc)
    this.drawImage = function () {
        if (this.video != null) {
            this.context.drawImage(this.video.image,0,0,this.canvas.width,this.canvas.height);//must draw to the size of the canvas
            if (this.results != null) {
                if (this.faceSelector.selectedFace != null &&
                    !this.video.hasPlayed) {
                    this.context.fillStyle = "rgba(128,128,128,0.5)"; // mid grey
                    this.context.fillRect(0,0,170,127);
                    return "grey"; // for testing
                } else if (this.video.vidID == this.results.getHappy()) {
                    this.context.globalAlpha = 0.5;
                    this.context.drawImage(this.happyOver, 0, 0, 170, 127);
                    this.context.globalAlpha = 1.0;
                    return "green"; // for testing
                } else if (this.video.vidID == this.results.getSad()) {
                    this.context.globalAlpha = 0.5;
                    this.context.drawImage(this.sadOver, 0, 0, 170, 127);
                    this.context.globalAlpha = 1.0;
                    return "red"; // for testing
                }
                return "clear"; // for testing
            } else {
                return "no results"; // for testing
            }
        } else {
            return false;
        }
    };

    var self = this;
	//handles mouse clicking on the thumbnails
    this.thumbnailClickHandler = function () {
		//depends on 
        if (self.videoSelector == null || self.faceSelector == null ||
            self.results == null || self.video == null) {
            return false; // for testing
        }

        // no face selected; choosing a video to play
        if (self.faceSelector.selectedFace == null) {
            self.videoSelector.pos = self.pos;
            self.videoSelector.loadVideo();
            return "no face"; // for testing
        }

        // liking / disliking a video that hasn't been played yet
        if (!self.video.hasPlayed) {
            console.log("You haven't watched that video!");
            self.faceSelector.deselectFace();
            return "not played"; // for testing
        // set happy video
        } else if ( self.faceSelector.selectedFace == "happy" &&
                    self.video.vidID != self.results.getSad()) {
            self.results.setHappy(self.pos);
            self.faceSelector.deselectFace();
            return "happy"; // for testing
        // set sad video
        } else if ( self.faceSelector.selectedFace == "sad" &&
                    self.video.vidID != self.results.getHappy()) {
            self.results.setSad(self.pos);
            self.faceSelector.deselectFace();
            return "sad"; // for testing
        } else {
            self.faceSelector.deselectFace();
            return "bad choice"; // for testing
        }
    };

    this.canvas.addEventListener("click", function () {
        self.thumbnailClickHandler();
    }, false);
};

module.exports.Thumbnail = Thumbnail;
