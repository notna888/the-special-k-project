/*
Object represents a particular video.
Holds the thumbnail image and 'played' state. 
*/
var util = require("./util.js");

var Video = function (vidID) {
    this.vidID = vidID;//unique video ID
    this.image = new Image();//thumbnail img
    this.thumbnail = null;

    this.src = util.getVideoPath(vidID);//video file location
    this.image.src = util.getThumbnailPath(vidID);//video thumbnail location

    this.hasPlayed = false;//whether a video has been played or not

    var self = this;
    this.image.onload = function () {
        if (self.thumbnail != null) {
            self.thumbnail.drawImage();//only draws image After it has finished loading
        }
    };
};

module.exports.Video = Video;
