/*
Object for the progress bar indicating the video number or whatever

*/
var progress = function () {
    this.bar = document.getElementById("loadingbar");
    this.numVideos = 21-1;//number of sets technically (minus 1)
    this.length = 420;//length of the bar itself
    this.height = 42;//height of the bar
    this.inc = this.length / this.numVideos;
    this.context = this.bar.getContext("2d");
    this.position = 0;

}
progress.prototype.draw = function () {
    //first clear off last rectangle (needed for decrementing)
    this.context.clearRect(0, 0, this.length, this.height);

    //using a linear gradient for added fancyness
    var gradient = this.context.createLinearGradient(0,0,this.length,this.height);
    gradient.addColorStop(0, '#33CC33');
    gradient.addColorStop(1, '#004C00');
    this.context.fillStyle = gradient;
    this.context.fillRect(0, 0, this.position * this.inc, this.height)
}
progress.prototype.increment = function () {
    this.position += 1;
}
progress.prototype.decrement = function () {
    this.position -= 1;
}

//expose to the module
module.exports.progress = progress;



