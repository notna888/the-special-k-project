/*
Video Selector object is the element that delegates the video playing functionality.
Loads, plays, pauses, each video. 
*/

//necessary assisting functions
var util = require("./util.js");

var VideoSelector = function (videos, thumbnails, results, loadingBar) {
    this.set = 0;//the set of 5 videos to be playing
    this.pos = 0;//the position of the video in the set (0-4)

    this.videos = videos;//set of videos
    this.thumbnails = thumbnails;//thumbnails associated with each video
    this.results = results;//the videos that were liked/disliked
    this.lBar = loadingBar;//the bar taht shows the progress of the set number out of the total number of sets.

    this.nextButton = document.getElementById("next");//reference to the next button html object
    this.prevButton = document.getElementById("previous");//reference to the previous button html object

    this.videoPlayer = document.getElementById("vidbox");//reference to the video html object
    this.videoPlayer.autoplay = true;//will automatically play each video.

    this.allPlayed = function () {//called once all videos have been viewed to the end
        for (var pos = 0; pos < util.numThumbnails; pos++) {
            var video = this.videos[util.getVideoID(this.set, pos)];
            if (video.hasPlayed == false) {
                return false;
            }
        }
        return true;
    };

    this.loadVideo = function () {//loads a particular video to the video object.
        var currentVideo = this.videos[util.getVideoID(this.set, this.pos)];
        this.videoPlayer.src = currentVideo.src;
    };

    this.loadThumbnails = function () {//changes the thumbnails
        for (var pos = 0;pos < util.numThumbnails; pos++) {
            var video = this.videos[util.getVideoID(this.set, pos)];
            this.thumbnails[pos].setVideo(video);
        }
    };
    var self = this;
    this.nextClickHandler = function () {//manages the series of operations 
        if (!self.allPlayed()) {//can't go to next button unless all vids have been viewed
            console.log("Haven't played all videos!");
			alert("You must have played all videos through to the end");
        } else if (self.results.getHappy() == null || self.results.getSad() == null) {//need to have liked and disliked a vid
            console.log("Haven't got 1 happy and 1 sad video!");
			alert("Need to Most-want and Least-want a video");
        } else if (self.set < util.numSets - 1) {
            //play transition animation:

            transAnimation();
            self.set++;
            self.pos = 0;
            self.loadVideo();
            self.loadThumbnails();
            self.lBar.increment();
            self.lBar.draw();
        } else if (self.set == util.numSets-1) {
            console.log("Go to results page!");
            console.log(self.results.calculate());
            window.location.href = "./results.html";
        }
    };

	//deals with the previous button and what changes happen on mouse press
    this.prevClickHandler = function () {
        if (self.set > 0) {
            self.set--;
            self.pos = 0;
            self.loadVideo();
            self.loadThumbnails();
            self.lBar.decrement();
            self.lBar.draw();
        } else {
			alert("Can't go back futher!");
        }
    };
	
	//handles what happens when a video finishes playing. 
    this.endVideoHandler = function () {
        var currentVideo = self.videos[util.getVideoID(self.set, self.pos)];
        currentVideo.hasPlayed = true;//sets this video to have been 'played'
        currentVideo.thumbnail.drawImage();//draw the thumbnail again (removes grey shadow if exists)
    };
	
	//attaching the mouse events to these
    this.nextButton.addEventListener("click", this.nextClickHandler);
    this.prevButton.addEventListener("click", this.prevClickHandler);
    this.videoPlayer.addEventListener("ended", this.endVideoHandler);
};

//the function handles the transition 'animation' that runs when a new set of videos is loaded. 
function transAnimation(c) {
	//this is kinda hacky but draws a canvas over the thumbnails and fades out, 
	//revealing below the new thumbnails. 
    var c = document.getElementById("transitionc");//gets the transition canvas
    c.width = 200;
    c.height = 680;
	//called many times over a second (aboutish)
    function fadeOutRectangle(x, y, w, h, r, g, b) {
        var steps = 40,
            dr = (255 - r) / steps,
            dg = (255 - g) / steps,
            db = (255 - b) / steps,
            i = 0,
            interval = setInterval(function () {
                c.getContext("2d").clearRect(0, 0, w, h);
                c.getContext("2d").fillStyle = 'rgba(' + r + ',' + g + ',' + b + ',' + (1-((1/steps)*i)) + ')';
                c.getContext("2d").fillRect(x, y, w, h);
                i++;
                if (i === steps) {
                    clearInterval(interval);
                    c.height = 0;
                    c.width = 0;
                }

            }, 30);
    }
	//calls function to fade in a new set of thumbnails. 
    fadeOutRectangle(0, 0, c.width, c.height, 135, 206, 250);
    
}

module.exports.VideoSelector = VideoSelector;
