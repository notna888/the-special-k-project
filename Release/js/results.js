/*
Object maintains the list of which videos were liked or disliked
*/

var util = require("./util.js");

var Results = function () {
    this.happy = [];//list of liked
    this.sad = [];//list of disliked
    for (var set = 0; set < util.numSets; set++) {
        this.happy.push(null);
        this.sad.push(null);
    }

    this.videoSelector = null;
	
	//function called when an particular video is liked
    this.setHappy = function (pos) {
        if (this.videoSelector != null) {
            var set = this.videoSelector.set;
            this.happy[set] = util.getVideoID(set, pos);
            return true;
        }
        return false;
    };
	
	//funciton called when a particular video is disliked
    this.setSad = function (pos) {
        if(this.videoSelector != null) {
            var set = this.videoSelector.set;
            this.sad[set] = util.getVideoID(set, pos);
            return true;
        }
        return false;
    };

	//gets a 'liked' video at a particular index, from a set. 
    this.getHappy = function () {
        if (this.videoSelector != null) {
            return this.happy[this.videoSelector.set];
        }
        return null;
    };
	
	//gets a disliked video from a particular set.
    this.getSad = function () {
        if (this.videoSelector != null) {
            return this.sad[this.videoSelector.set];
        }
        return null;
    };
	
	//calculates each video's like or dislike 'rating' 
    this.calculate = function () {
        var results = [];
        for (var vidID = 0; vidID < util.numVideos; vidID++) {
            results.push(0);
        }

        for (var set = 0; set < util.numSets; set++) {
            results[this.happy[set]] += 1;
            results[this.sad[set]]   -= 1
        }
		//sends it to the sessionStorage to be loaded in the results.html

        for (var vidID = 0; vidID < util.numVideos; vidID++) {
            results[vidID] = parseFloat((results[vidID] / 5).toPrecision(2));
        }

        sessionStorage.result = JSON.stringify(results);
        return results;
    };
};

module.exports.Results = Results;
