/*
Bunch of utility functions and data for operation of the project.
*/

//the order in which videos appear in sets and in what order the sets are in.
var videoOrder = [
    [18, 2, 6,14,10], [ 6, 1, 7, 8, 9], [16,11, 3, 6,21],
    [15, 6,20, 5,13], [19, 4,17,12, 6], [ 5, 3, 4, 2, 1],
    [ 2,19,15,11, 7], [13,17,21, 9, 2], [ 8,12, 2,20,16],
    [12,10, 1,13,11], [20, 7,10, 3,17], [10, 5, 9,16,19],
    [21,15, 8,10, 4], [ 1,21,19,18,20], [ 4,13,16, 7,18],
    [ 3, 9,18,15,12], [11,18, 5,17, 8], [17,16,14, 1,15],
    [ 7,14,12,21, 5], [ 9,20,11, 4,14], [14, 8,13,19, 3]
];

//this is handy if everything has a different root folder but all g for now
window.path = "";

//gets the id of a video at that set and position in the set. 
//21 sets and 5 positions in each set. 
var getVideoID = function (set, pos) {
    return videoOrder[set][pos]-1;
};

var numVideos = 21;

var numSets = 21;

var numThumbnails = 5;


//the file path for a particular video. 
//lingo is a language selection and changes depending on the language selected at the beginning
var getVideoPath = function (vidID) {
    if (sessionStorage.lingo == "" || sessionStorage.lingo === undefined) {
        return window.path+"assets/videos/vid ("+(vidID+1)+").m4v";
    } else {
        return window.path+"assets/videos/"+sessionStorage.lingo+"/vid ("+(vidID+1)+").m4v";
    }
};

//the file path for a particular thumbnail image. 
//lingo is the language selection depending on the selected language.
var getThumbnailPath = function (vidID) {
    if (sessionStorage.lingo == "" || sessionStorage.lingo === undefined) {
        return window.path+"assets/thumbnails/thumbnail"+(vidID+1)+".jpg";
    } else {
        return window.path+"assets/thumbnails/"+sessionStorage.lingo+"/thumbnail"+(vidID+1)+".jpg";
    }
};

module.exports.getVideoID = getVideoID;
module.exports.numVideos = numVideos;
module.exports.numSets = numSets;
module.exports.numThumbnails = numThumbnails;
module.exports.getVideoPath = getVideoPath;
module.exports.getThumbnailPath = getThumbnailPath;
