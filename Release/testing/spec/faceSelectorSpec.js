// require is needed only when testing with karma
if (typeof __karma__ != "undefined") {
    var FaceSelector = require("../../js/faceSelector.js").FaceSelector;

    var setup = require("./setup.js");
    var setup_thumbnail = setup.setup_thumbnail;
    var setup_videoSelector = setup.setup_videoSelector;
    var setup_faceSelector = setup.setup_faceSelector;
    var Mocks = setup.Mocks;
}

setup_thumbnail();
setup_videoSelector();
setup_faceSelector();

var mocks = new Mocks();
var thumbnails = mocks.thumbnails;

describe("faceSelector.js", function () {
    it("click handlers", function () {
        var faceSelector = new FaceSelector(thumbnails);
        
        expect(faceSelector.selectedFace).toBe(null);
        faceSelector.happyClickHandler(); // select happy
        expect(faceSelector.selectedFace).toBe("happy");
        faceSelector.sadClickHandler(); // select sad
        expect(faceSelector.selectedFace).toBe("sad");
        faceSelector.sadClickHandler(); // deselect
        expect(faceSelector.selectedFace).toBe(null);
        faceSelector.happyClickHandler(); // select happy
        expect(faceSelector.selectedFace).toBe("happy");
        faceSelector.happyClickHandler(); // deselect
        expect(faceSelector.selectedFace).toBe(null);
    });
});
