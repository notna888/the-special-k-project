// require is needed only when testing with karma
if (typeof __karma__ != "undefined") {
    var Results = require("../../js/results.js").Results;

    var util = require("../../js/util.js");

    var setup = require("./setup.js");
    var setup_thumbnail = setup.setup_thumbnail;
    var setup_videoSelector = setup.setup_videoSelector;
    var setup_faceSelector = setup.setup_faceSelector;
    var Mocks = setup.Mocks;
}

setup_thumbnail();
setup_videoSelector();
setup_faceSelector();

var mocks = new Mocks();
var videoSelector = mocks.videoSelector;

var emptyArray = [];
var zeroArray = [];
for (var set = 0; set < util.numSets; set++) {
    emptyArray.push(null);
    zeroArray.push(0);
}

var testHappy = [17,null,null,null,null,null,null,
                null,null,null,null,null,null,null,
                null,null,null,null,null,null,null];
var testSad = [1,null,null,null,null,null,null,
                null,null,null,null,null,null,null,
                null,null,null,null,null,null,null];

var calculated0 = [   0,-0.2,   0,   0,   0,   0,   0,
                      0,   0,   0,   0,   0,   0,   0,
                      0,   0,   0, 0.2,   0,   0,   0];

var happyInput1 = [  1, 0, 2, 4, 3, 1, 1, 8, 1, 0, 2, 4, 7, 0, 3, 2, 4, 0, 6, 3, 7];
var sadInput1 = [   17, 7,15,19,18, 4,18,20,19,12,19,15,20,20,15,17,17,16,20,19,18];
var happyInput2 = [  5, 6, 2,14,16, 3,14,16, 7,11, 9, 8,14,18,12, 8,10,13,11,10,12];
var sadInput2 = [   13, 5, 5, 5,11, 2,10,12,11, 9, 6, 9, 9,17, 6,14,16,15,13, 8,13];

var calculated1 = [ 0.8, 0.8, 0.6, 0.6, 0.4,   0, 0.2,
                    0.2, 0.2,   0,   0,   0,-0.2,   0,
                      0,-0.6,-0.2,-0.6,-0.6,-0.8,-0.8]

var calculated2 = [   0,   0,   0, 0.2,   0,-0.4,-0.2,
                    0.2, 0.2,-0.4, 0.2,   0, 0.2,-0.4,
                    0.4,-0.2, 0.2,-0.2, 0.2,   0,   0]

// test suite
describe("results.js", function () {
    it("just results", function () {
        var results = new Results();

        expect(results.happy.length).toBe(emptyArray.length);
        expect(results.happy.length).toBe(util.numSets);
        expect(results.sad.length).toBe(emptyArray.length);
        expect(results.sad.length).toBe(util.numSets);

        for(var set = 0; set < util.numSets; set++) {
            expect(results.happy[set]).toBe(emptyArray[set]);
            expect(results.sad[set]).toBe(emptyArray[set]);
        }

        expect(results.videoSelector).toBe(null);
        expect(results.setHappy(0)).toBe(false);
        expect(results.setSad(0)).toBe(false);
        expect(results.getHappy()).toBe(null);
        expect(results.getSad()).toBe(null);

        var calculated_results = results.calculate();
        expect(calculated_results.length).toBe(zeroArray.length);
        expect(calculated_results.length).toBe(util.numVideos);

        for(var set = 0; set < util.numVideos; set++) {
            expect(calculated_results[set]).toBe(zeroArray[set]);
        }
    });
    it("results with videoSelector", function () {
        var results = new Results();
        results.videoSelector = videoSelector;
        results.videoSelector.set = 0;
        results.videoSelector.pos = 0;

        expect(results.happy.length).toBe(emptyArray.length);
        expect(results.happy.length).toBe(util.numSets);
        expect(results.sad.length).toBe(emptyArray.length);
        expect(results.sad.length).toBe(util.numSets);

        for(var set = 0; set < util.numSets; set++) {
           expect(results.happy[set]).toBe(emptyArray[set]);
            expect(results.sad[set]).toBe(emptyArray[set]);
        }

        expect(results.videoSelector).toBe(videoSelector);
        expect(results.setHappy(0)).toBe(true);
        expect(results.setSad(1)).toBe(true);
        expect(results.getHappy()).toBe(17);
        expect(results.getSad()).toBe(1);

        var calculated_results = results.calculate();
        expect(calculated_results.length).toBe(util.numVideos);
        expect(calculated_results.length).toBe(calculated0.length);

        for(var set = 0; set < util.numVideos; set++) {
            expect(calculated_results[set]).toBe(calculated0[set]);
        }

    });
    it("results.calculate() test 1", function () {
        var results = new Results();
        results.videoSelector = videoSelector;
        results.videoSelector.set = 0;
        results.videoSelector.pos = 0;

        results.happy = happyInput1;
        results.sad = sadInput1;

        var calculated_results = results.calculate();
        expect(calculated_results.length).toBe(util.numVideos);
        expect(calculated_results.length).toBe(calculated1.length);

        for(var set = 0; set < util.numVideos; set++) {
            expect(calculated_results[set]).toBe(calculated1[set]);
        }
    });
    it("results.calculate() test 2", function () {
        var results = new Results();
        results.videoSelector = videoSelector;
        results.videoSelector.set = 0;
        results.videoSelector.pos = 0;

        results.happy = happyInput2;
        results.sad = sadInput2;

        var calculated_results = results.calculate();
        expect(calculated_results.length).toBe(util.numVideos);
        expect(calculated_results.length).toBe(calculated2.length);

        for(var set = 0; set < util.numVideos; set++) {
            expect(calculated_results[set]).toBe(calculated2[set]);
        }
    });
});
