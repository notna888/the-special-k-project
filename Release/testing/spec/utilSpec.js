// require is needed only when testing with karma
if(typeof __karma__ != "undefined") {
    var util = require("../../js/util.js");
    var getVideoID = util.getVideoID;
    var numVideos = util.numVideos;
    var numSets = util.numSets;
    var numThumbnails = util.numThumbnails;
    var getVideoPath = util.getVideoPath;
    var getThumbnailPath = util.getThumbnailPath;
}

window.path = "../../";

// test suite
describe("util.js", function () {
    it("getVideoID", function () {
        expect(getVideoID(0,0)).toBe(17);
        expect(getVideoID(2,2)).toBe(2);
        expect(getVideoID(20,4)).toBe(2);
        expect(getVideoID(7,2)).toBe(20);
        expect(getVideoID(17,3)).toBe(0);
        expect(getVideoID(8,1)).toBe(11);
        expect(getVideoID(10,0)).toBe(19);
        expect(getVideoID(14,4)).toBe(17);
    });
    it("numVideos", function () {
        expect(numVideos).toBe(21);
    });
    it("numSets", function () {
        expect(numSets).toBe(21);
    });
    it("numThumbnails", function () {
        expect(numThumbnails).toBe(5);
    });
    it("getVideoPath - no lingo", function () {
        sessionStorage.lingo = "";
        expect(getVideoPath(0)).toBe(window.path+"assets/videos/vid (1).m4v");
        expect(getVideoPath(7)).toBe(window.path+"assets/videos/vid (8).m4v");
        expect(getVideoPath(12)).toBe(window.path+"assets/videos/vid (13).m4v");
        expect(getVideoPath(19)).toBe(window.path+"assets/videos/vid (20).m4v");
    });
    it("getThumbnailPath - no lingo", function () {
        sessionStorage.lingo = "";
        expect(getThumbnailPath(3)).toBe(window.path+"assets/thumbnails/thumbnail4.jpg");
        expect(getThumbnailPath(14)).toBe(window.path+"assets/thumbnails/thumbnail15.jpg");
        expect(getThumbnailPath(6)).toBe(window.path+"assets/thumbnails/thumbnail7.jpg");
        expect(getThumbnailPath(17)).toBe(window.path+"assets/thumbnails/thumbnail18.jpg");
    });
    it("getVideoPath - with lingo", function () {
        sessionStorage.lingo = "test";
        expect(getVideoPath(2)).toBe(window.path+"assets/videos/test/vid (3).m4v");
        expect(getVideoPath(9)).toBe(window.path+"assets/videos/test/vid (10).m4v");
        expect(getVideoPath(15)).toBe(window.path+"assets/videos/test/vid (16).m4v");
        expect(getVideoPath(20)).toBe(window.path+"assets/videos/test/vid (21).m4v");
    });
    it("getThumbnailPath - with lingo", function () {
        sessionStorage.lingo = "test";
        expect(getThumbnailPath(1)).toBe(window.path+"assets/thumbnails/test/thumbnail2.jpg");
        expect(getThumbnailPath(18)).toBe(window.path+"assets/thumbnails/test/thumbnail19.jpg");
        expect(getThumbnailPath(11)).toBe(window.path+"assets/thumbnails/test/thumbnail12.jpg");
        expect(getThumbnailPath(5)).toBe(window.path+"assets/thumbnails/test/thumbnail6.jpg");
    });
});

