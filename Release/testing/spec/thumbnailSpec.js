// require is needed only when testing with karma
if (typeof __karma__ != "undefined") {
    var Thumbnail = require("../../js/thumbnail.js").Thumbnail;
    var Video = require("../../js/video.js").Video;

    var setup = require("./setup.js");
    var setup_thumbnail = setup.setup_thumbnail;
    var setup_videoSelector = setup.setup_videoSelector;
    var setup_faceSelector = setup.setup_faceSelector;
    var Mocks = setup.Mocks;
}

window.path = "../../";

setup_thumbnail();
setup_videoSelector();
setup_faceSelector();

var mocks = new Mocks();
var results = mocks.results;
var videoSelector = mocks.videoSelector;
var faceSelector = mocks.faceSelector;

// test suite
describe("thumbnail.js", function () {
    it("just thumbnail", function () {
        var thumbnail = new Thumbnail(0);

        // initial tests
        expect(thumbnail.pos).toBe(0);
        expect(thumbnail.canvas.width).toBe(170);
        expect(thumbnail.canvas.height).toBe(127);
        expect(thumbnail.canvas.id).toBe("thumb1c");
        expect(thumbnail.videoSelector).toBe(null);
        expect(thumbnail.faceSelector).toBe(null);
        expect(thumbnail.results).toBe(null);
        expect(thumbnail.video).toBe(null);
        expect(thumbnail.happyOver.src).toMatch(/\/assets\/images\/happyover\.png$/);
        expect(thumbnail.sadOver.src).toMatch(/\/assets\/images\/sadover\.png$/);

        // more tests
        expect(thumbnail.drawImage()).toBe(false);
        expect(thumbnail.thumbnailClickHandler()).toBe(false);
    });
    it("thumbnail with video", function () {
        var thumbnail = new Thumbnail(1);
        var video = new Video(11);
        thumbnail.setVideo(video);

        // initial tests
        expect(thumbnail.pos).toBe(1);
        expect(thumbnail.canvas.width).toBe(170);
        expect(thumbnail.canvas.height).toBe(127);
        expect(thumbnail.canvas.id).toBe("thumb2c");
        expect(thumbnail.videoSelector).toBe(null);
        expect(thumbnail.faceSelector).toBe(null);
        expect(thumbnail.results).toBe(null);
        expect(thumbnail.video).toBe(video);
        expect(thumbnail.happyOver.src).toMatch(/\/assets\/images\/happyover\.png$/);
        expect(thumbnail.sadOver.src).toMatch(/\/assets\/images\/sadover\.png$/);

        // more tests
        expect(thumbnail.drawImage()).toBe("no results");
        expect(thumbnail.thumbnailClickHandler()).toBe(false);
    });
    it("thumbnail with videoSelector, faceSelector, results, video", function () {
        var thumbnail = new Thumbnail(2);
        var video = new Video(5);
        thumbnail.setVideo(video);
        thumbnail.videoSelector = videoSelector;
        thumbnail.faceSelector = faceSelector;
        thumbnail.results = results;

        // initial tests
        expect(thumbnail.pos).toBe(2);
        expect(thumbnail.canvas.width).toBe(170);
        expect(thumbnail.canvas.height).toBe(127);
        expect(thumbnail.canvas.id).toBe("thumb3c");
        expect(thumbnail.videoSelector).toBe(videoSelector);
        expect(thumbnail.faceSelector).toBe(faceSelector);
        expect(thumbnail.results).toBe(results);
        expect(thumbnail.video).toBe(video);
        expect(thumbnail.happyOver.src).toMatch(/\/assets\/images\/happyover\.png$/);
        expect(thumbnail.sadOver.src).toMatch(/\/assets\/images\/sadover\.png$/);

        // more tests
        thumbnail.results.happy[0] = null;
        thumbnail.results.sad[0] = null;
        thumbnail.faceSelector.selectedFace = null;
        thumbnail.video.hasPlayed = false;
        expect(thumbnail.drawImage()).toBe("clear");
        expect(thumbnail.thumbnailClickHandler()).toBe("no face");
        expect(thumbnail.results.getHappy()).toBe(null);
        expect(thumbnail.results.getSad()).toBe(null);

        thumbnail.results.happy[0] = null;
        thumbnail.results.sad[0] = null;
        thumbnail.faceSelector.selectedFace = null;
        thumbnail.video.hasPlayed = true;
        expect(thumbnail.drawImage()).toBe("clear");
        expect(thumbnail.thumbnailClickHandler()).toBe("no face");
        expect(thumbnail.results.getHappy()).toBe(null);
        expect(thumbnail.results.getSad()).toBe(null);

        thumbnail.results.happy[0] = null;
        thumbnail.results.sad[0] = null;
        thumbnail.faceSelector.selectedFace = "happy";
        thumbnail.video.hasPlayed = false;
        expect(thumbnail.drawImage()).toBe("grey");
        expect(thumbnail.thumbnailClickHandler()).toBe("not played");
        expect(thumbnail.results.getHappy()).toBe(null);
        expect(thumbnail.results.getSad()).toBe(null);
        expect(thumbnail.thumbnailClickHandler()).toBe("no face");

        thumbnail.results.happy[0] = null;
        thumbnail.results.sad[0] = null;
        thumbnail.faceSelector.selectedFace = "happy";
        thumbnail.video.hasPlayed = true;
        expect(thumbnail.drawImage()).toBe("clear");
        expect(thumbnail.thumbnailClickHandler()).toBe("happy");
        expect(thumbnail.results.getHappy()).toBe(5);
        expect(thumbnail.results.getSad()).toBe(null);
        expect(thumbnail.thumbnailClickHandler()).toBe("no face");

        thumbnail.results.happy[0] = null;
        thumbnail.results.sad[0] = null;
        thumbnail.faceSelector.selectedFace = "sad";
        thumbnail.video.hasPlayed = false;
        expect(thumbnail.drawImage()).toBe("grey");
        expect(thumbnail.thumbnailClickHandler()).toBe("not played");
        expect(thumbnail.results.getHappy()).toBe(null);
        expect(thumbnail.results.getSad()).toBe(null);
        expect(thumbnail.thumbnailClickHandler()).toBe("no face");

        thumbnail.results.happy[0] = null;
        thumbnail.results.sad[0] = null;
        thumbnail.faceSelector.selectedFace = "sad";
        thumbnail.video.hasPlayed = true;
        expect(thumbnail.drawImage()).toBe("clear");
        expect(thumbnail.thumbnailClickHandler()).toBe("sad");
        expect(thumbnail.results.getHappy()).toBe(null);
        expect(thumbnail.results.getSad()).toBe(5);
        expect(thumbnail.thumbnailClickHandler()).toBe("no face");

        // this thumbnail is happy
        thumbnail.results.happy[0] = 5;
        thumbnail.results.sad[0] = null;
        thumbnail.faceSelector.selectedFace = null;
        thumbnail.video.hasPlayed = false;
        expect(thumbnail.drawImage()).toBe("green");
        expect(thumbnail.thumbnailClickHandler()).toBe("no face");
        expect(thumbnail.results.getHappy()).toBe(5);
        expect(thumbnail.results.getSad()).toBe(null);

        thumbnail.results.happy[0] = 5;
        thumbnail.results.sad[0] = null;
        thumbnail.faceSelector.selectedFace = null;
        thumbnail.video.hasPlayed = true;
        expect(thumbnail.drawImage()).toBe("green");
        expect(thumbnail.thumbnailClickHandler()).toBe("no face");
        expect(thumbnail.results.getHappy()).toBe(5);
        expect(thumbnail.results.getSad()).toBe(null);

        thumbnail.results.happy[0] = 5;
        thumbnail.results.sad[0] = null;
        thumbnail.faceSelector.selectedFace = "happy";
        thumbnail.video.hasPlayed = false;
        expect(thumbnail.drawImage()).toBe("grey");
        expect(thumbnail.thumbnailClickHandler()).toBe("not played");
        expect(thumbnail.results.getHappy()).toBe(5);
        expect(thumbnail.results.getSad()).toBe(null);
        expect(thumbnail.thumbnailClickHandler()).toBe("no face");

        thumbnail.results.happy[0] = 5;
        thumbnail.results.sad[0] = null;
        thumbnail.faceSelector.selectedFace = "happy";
        thumbnail.video.hasPlayed = true;
        expect(thumbnail.drawImage()).toBe("green");
        expect(thumbnail.thumbnailClickHandler()).toBe("happy");
        expect(thumbnail.results.getHappy()).toBe(5);
        expect(thumbnail.results.getSad()).toBe(null);
        expect(thumbnail.thumbnailClickHandler()).toBe("no face");

        thumbnail.results.happy[0] = 5;
        thumbnail.results.sad[0] = null;
        thumbnail.faceSelector.selectedFace = "sad";
        thumbnail.video.hasPlayed = false;
        expect(thumbnail.drawImage()).toBe("grey");
        expect(thumbnail.thumbnailClickHandler()).toBe("not played");
        expect(thumbnail.results.getHappy()).toBe(5);
        expect(thumbnail.results.getSad()).toBe(null);
        expect(thumbnail.thumbnailClickHandler()).toBe("no face");

        thumbnail.results.happy[0] = 5;
        thumbnail.results.sad[0] = null;
        thumbnail.faceSelector.selectedFace = "sad";
        thumbnail.video.hasPlayed = true;
        expect(thumbnail.drawImage()).toBe("green");
        expect(thumbnail.thumbnailClickHandler()).toBe("bad choice");
        expect(thumbnail.results.getHappy()).toBe(5);
        expect(thumbnail.results.getSad()).toBe(null);
        expect(thumbnail.thumbnailClickHandler()).toBe("no face");

        // this thumbnail is sad
        thumbnail.results.happy[0] = null;
        thumbnail.results.sad[0] = 5;
        thumbnail.faceSelector.selectedFace = null;
        thumbnail.video.hasPlayed = false;
        expect(thumbnail.drawImage()).toBe("red");
        expect(thumbnail.thumbnailClickHandler()).toBe("no face");
        expect(thumbnail.results.getHappy()).toBe(null);
        expect(thumbnail.results.getSad()).toBe(5);

        thumbnail.results.happy[0] = null;
        thumbnail.results.sad[0] = 5;
        thumbnail.faceSelector.selectedFace = null;
        thumbnail.video.hasPlayed = true;
        expect(thumbnail.drawImage()).toBe("red");
        expect(thumbnail.thumbnailClickHandler()).toBe("no face");
        expect(thumbnail.results.getHappy()).toBe(null);
        expect(thumbnail.results.getSad()).toBe(5);

        thumbnail.results.happy[0] = null;
        thumbnail.results.sad[0] = 5;
        thumbnail.faceSelector.selectedFace = "happy";
        thumbnail.video.hasPlayed = false;
        expect(thumbnail.drawImage()).toBe("grey");
        expect(thumbnail.thumbnailClickHandler()).toBe("not played");
        expect(thumbnail.results.getHappy()).toBe(null);
        expect(thumbnail.results.getSad()).toBe(5);
        expect(thumbnail.thumbnailClickHandler()).toBe("no face");

        thumbnail.results.happy[0] = null;
        thumbnail.results.sad[0] = 5;
        thumbnail.faceSelector.selectedFace = "happy";
        thumbnail.video.hasPlayed = true;
        expect(thumbnail.drawImage()).toBe("red");
        expect(thumbnail.thumbnailClickHandler()).toBe("bad choice");
        expect(thumbnail.results.getHappy()).toBe(null);
        expect(thumbnail.results.getSad()).toBe(5);
        expect(thumbnail.thumbnailClickHandler()).toBe("no face");

        thumbnail.results.happy[0] = null;
        thumbnail.results.sad[0] = 5;
        thumbnail.faceSelector.selectedFace = "sad";
        thumbnail.video.hasPlayed = false;
        expect(thumbnail.drawImage()).toBe("grey");
        expect(thumbnail.thumbnailClickHandler()).toBe("not played");
        expect(thumbnail.results.getHappy()).toBe(null);
        expect(thumbnail.results.getSad()).toBe(5);
        expect(thumbnail.thumbnailClickHandler()).toBe("no face");

        thumbnail.results.happy[0] = null;
        thumbnail.results.sad[0] = 5;
        thumbnail.faceSelector.selectedFace = "sad";
        thumbnail.video.hasPlayed = true;
        expect(thumbnail.drawImage()).toBe("red");
        expect(thumbnail.thumbnailClickHandler()).toBe("sad");
        expect(thumbnail.results.getHappy()).toBe(null);
        expect(thumbnail.results.getSad()).toBe(5);
        expect(thumbnail.thumbnailClickHandler()).toBe("no face");
    });
});
