// require is needed only when testing with karma
if (typeof __karma__ != "undefined") {
    var VideoSelector = require("../../js/videoSelector.js").VideoSelector;

    var util = require("../../js/util.js");

    var setup = require("./setup.js");
    var setup_thumbnail = setup.setup_thumbnail;
    var setup_videoSelector = setup.setup_videoSelector;
    var setup_faceSelector = setup.setup_faceSelector;
    var Mocks = setup.Mocks;
}

setup_thumbnail();
setup_videoSelector();
setup_faceSelector();

var mocks = new Mocks();
var videos = mocks.videos;
var thumbnails = mocks.thumbnails;
var results = mocks.results;
var loadingBar = mocks.loadingBar;

// test suite
describe("videoSelector.js", function () {
    it("starting point", function () {
        var videoSelector = new VideoSelector(videos, thumbnails, results, loadingBar);
        expect(videoSelector.set).toBe(0);
        expect(videoSelector.pos).toBe(0);
        expect(videoSelector.videos).toBe(videos);
        expect(videoSelector.thumbnails).toBe(thumbnails);
        expect(videoSelector.results).toBe(results);
        expect(videoSelector.lBar).toBe(loadingBar);
    });
    it("allPlayed", function () {
        var videoSelector = new VideoSelector(videos, thumbnails, results, loadingBar);

        expect(videoSelector.allPlayed()).toBe(false);

        for (var pos = 0; pos < util.numThumbnails; pos++) {
            var vidID = util.getVideoID(0, pos);
            videoSelector.videos[vidID].hasPlayed = true;
        }
        expect(videoSelector.allPlayed()).toBe(true);
    });
});
