// require is needed only when testing with karma
if (typeof __karma__ != "undefined") {
    var Video = require("../../js/video.js").Video;
    var Thumbnail = require("../../js/thumbnail.js").Thumbnail;

    var setup = require("./setup.js");
    var setup_thumbnail = setup.setup_thumbnail;
}

window.path = "../../";
setup_thumbnail();

// test suite
describe("video.js", function () {
    it("video without thumbnail", function () {
        sessionStorage.lingo = "";

        var video = new Video(0);

        expect(video.vidID).toBe(0);
        expect(video.src).toMatch(/\/assets\/videos\/vid( |%20)\(1\)\.m4v$/);
        expect(video.image.src).toMatch(/\/assets\/thumbnails\/thumbnail1\.jpg$/);
        expect(video.thumbnail).toBe(null);
        expect(video.hasPlayed).toBe(false);
    });
    it("video with thumbnail", function () {
        sessionStorage.lingo = "";

        var video = new Video(5);
        var thumbnail = new Thumbnail(0);
        thumbnail.setVideo(video);

        expect(video.vidID).toBe(5);
        expect(video.src).toMatch(/\/assets\/videos\/vid( |%20)\(6\)\.m4v$/);
        expect(video.image.src).toMatch(/\/assets\/thumbnails\/thumbnail6\.jpg$/);
        expect(video.thumbnail).toBe(thumbnail);
        expect(video.hasPlayed).toBe(false);
    });
});

