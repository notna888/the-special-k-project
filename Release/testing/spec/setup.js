// require is needed only when testing with karma
if(typeof __karma__ != "undefined") {
    var util = require("../../js/util.js");

    var VideoSelector = require("../../js/videoSelector.js").VideoSelector;
    var FaceSelector = require("../../js/faceSelector.js").FaceSelector;
    var Video = require("../../js/video.js").Video;
    var Thumbnail = require("../../js/thumbnail.js").Thumbnail;
    var Results = require("../../js/results.js").Results;
    var LoadingBar = require("../../js/loadingBar.js").progress;
}

var setup_thumbnail = function () {
    var canvas1 = document.createElement("canvas");
    canvas1.width = "170";
    canvas1.height = "127";
    canvas1.id = "thumb1c";
    document.body.appendChild(canvas1);

    var canvas2 = document.createElement("canvas");
    canvas2.width = "170";
    canvas2.height = "127";
    canvas2.id = "thumb2c";
    document.body.appendChild(canvas2);

    var canvas3 = document.createElement("canvas");
    canvas3.width = "170";
    canvas3.height = "127";
    canvas3.id = "thumb3c";
    document.body.appendChild(canvas3);

    var canvas4 = document.createElement("canvas");
    canvas4.width = "170";
    canvas4.height = "127";
    canvas4.id = "thumb4c";
    document.body.appendChild(canvas4);

    var canvas5 = document.createElement("canvas");
    canvas5.width = "170";
    canvas5.height = "127";
    canvas5.id = "thumb5c";
    document.body.appendChild(canvas5);
}

var setup_videoSelector = function () {
    var loadingbar_canvas = document.createElement("canvas");
    loadingbar_canvas.id = "loadingbar";
    document.body.appendChild(loadingbar_canvas);

    var vidbox = document.createElement("video");
    vidbox.id = "vidbox";
    document.body.appendChild(vidbox);

    var next = document.createElement("image");
    next.id = "next";
    document.body.appendChild(next);

    var prev = document.createElement("image");
    prev.id = "previous";
    document.body.appendChild(prev);
}

var setup_faceSelector = function () {
    var happybutton = document.createElement("image");
    happybutton.id = "happy";
    document.body.appendChild(happybutton);

    var sadbutton = document.createElement("image");
    sadbutton.id = "sad";
    document.body.appendChild(sadbutton);
}

var Mocks = function () {
    this.videos = [];
    this.thumbnails = [];
    this.results = new Results();
    this.loadingBar = new LoadingBar();
    this.videoSelector = new VideoSelector(this.videos, this.thumbnails, this.results, this.loadingBar);
    this.faceSelector = new FaceSelector(this.thumbnails);
    this.results.videoSelector = this.videoSelector;

    for (var vidID = 0; vidID < util.numVideos; vidID++) {
        this.videos.push(new Video(vidID));
    }

    for (var pos = 0; pos < util.numThumbnails; pos++) {
        this.thumbnails.push(new Thumbnail(pos));
    }
}

module.exports.setup_thumbnail = setup_thumbnail;
module.exports.setup_videoSelector = setup_videoSelector;
module.exports.setup_faceSelector = setup_faceSelector;
module.exports.Mocks = Mocks;
