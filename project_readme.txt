# Project Information
## Unit:	CITS3200
## Group: 	K

#
## Deadlines
#
 - Deliverable A [Week 4] 5%
 	- Requirements Analysis Document [RAD]
 	- Planning and Estimation (in house)

 - Ethics Essay [Week 5] 25%
 	- Not a group assignment, but still important

- Deliverable B [Week 6?] 20%
	- Formal Project Plan
	- Skills Audit
	- Risk Analysis
	- Exploration of Use Cases
	- Signed off by client

 - Deliverable C [Week 10] 40%
 	- Project Code
 	- Testing done
 	- Documentation
 	- User Manual

 - Deliverable D [Week 12] 10%
 	- Speech/Presentation
 	- What went wrong
 	- What went right

#
## Contact Information
#
	Team Member		Skype Name			Student Number
   ======================================================
	 William O		William Owers 			21316094
	 Anton S		notna888 			21336394
	 Reece N		reece.como 			21108155
	 Lauren G		nightglyde 			21495335
	 Mathias G		mathiasgebauer			21321934
	 Wilsen L 		<none>				21105559

#
## Booked Hours and Minutes
#
	Booked Hours
   ==============
	Booked hours is split into weekly files, each with a sheet of personal hours for each of you. Only update your file (obvs)
	and at the end of the week on a friday afternoon i'll look through each, compile into the one file and send it to Mr Sir.

	Timesheets
   ==============
	The Timesheets folder just has the big time sheet spread sheet thing with the different project related activities. 
	We'll deal with that next week, i think i'll put one into a weekly folder and copy it to the next week one so we can see 
	what has changed week to week.

#
## Project Specifications (& Client Meeting)
#
	Overview
   ============
	Andriod & iPad (web too) app interface for
	interactive visual quiz, targeted at children.

	Design Considerations
   ============
	- Children are generally pretty good with tech (not joking)
	- The app should be fluid and easy to use (nice drag and drop, not clunky)
	- The presentation should be seperate from the API
		(So the code that processes and stores data should be entirely modular
		from the code that displays/interacts with that code) 

	User Input
   ==============
	Age & Gender
	Responses to 21 sets of 5 animations
		Drag and drop interfacing		(2 ea. good & bad)
		End and Thank You 	(email optional)

	Output To User/Admin
   ============
	<QSort of the end?>
	Send results off
	Output at the end

	Functionality 		Percieved Importance
   =========================================
  1. Downloaded			0.4 (40)
		Videos 
  2. iPad (1)			0.3 (30)
	   Android (2)		
	   Web App (3)
  3. Results O/P 		0.2 (20)
  4. Upload data +		0.1 (10)
		Offline		
