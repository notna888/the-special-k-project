// require is needed only when testing with karma
if (typeof __karma__ != "undefined") {
    var Video = require("../../js/video.js").Video;
    var Thumbnail = require("../../js/thumbnail.js").Thumbnail;
}

window.path = "../../";

// test suite
describe("video.js", function () {
    var video = new Video(0);
    console.log(video);
    it("preThumbnail", function () {
        expect(video.vidID).toBe(0);
        expect(video.image.src).toMatch(/\/assets\/thumbs\/vt( |%20)\(1\)\.jpg$/);
        expect(video.thumbnail).toBe(null);
        expect(video.src).toMatch(/\/assets\/vid( |%20)\(1\)\.m4v$/);
        expect(video.hasPlayed).toBe(false);
        expect(video.imageHasLoaded).toBe(false);
    });
    var canvas = document.createElement("canvas");
    canvas.width = "127";
    canvas.height = "170";
    canvas.id = "thumb1c";
    document.body.appendChild(canvas);
    video.thumbnail = new Thumbnail(0);
});
