// require is needed only when testing with karma
if(typeof __karma__ != "undefined") {
    var util = require("../../js/util.js");
    var getVideoID = util.getVideoID;
    var numVideos = util.numVideos;
    var numSets = util.numSets;
    var numThumbnails = util.numThumbnails;
    var getVideoPath = util.getVideoPath;
    var getThumbnailPath = util.getThumbnailPath;
}

window.path = "../../";

// test suite
describe("util.js", function () {
    it("getVideoID", function () {
        expect(getVideoID(0,0)).toBe(17);
        expect(getVideoID(2,2)).toBe(2);
        expect(getVideoID(20,4)).toBe(2);
        expect(getVideoID(7,2)).toBe(20);
        expect(getVideoID(17,3)).toBe(0);
        expect(getVideoID(8,1)).toBe(11);
        expect(getVideoID(10,0)).toBe(19);
        expect(getVideoID(14,4)).toBe(17);
    });
    it("numVideos", function () {
        expect(numVideos).toBe(21);
    });
    it("numSets", function () {
        expect(numSets).toBe(21);
    });
    it("numThumbnails", function () {
        expect(numThumbnails).toBe(5);
    });
    it("getVideoPath", function () {
        expect(getVideoPath(0)).toBe(window.path+"assets/vid (1).m4v");
        expect(getVideoPath(7)).toBe(window.path+"assets/vid (8).m4v");
        expect(getVideoPath(12)).toBe("../../assets/vid (13).m4v");
        expect(getVideoPath(19)).toBe("../../assets/vid (20).m4v");
    });
    it("getThumbnailPath", function () {
        expect(getThumbnailPath(3)).toBe(window.path+"assets/thumbs/vt (4).jpg");
        expect(getThumbnailPath(14)).toBe(window.path+"assets/thumbs/vt (15).jpg");
        expect(getThumbnailPath(6)).toBe("../../assets/thumbs/vt (7).jpg");
        expect(getThumbnailPath(17)).toBe("../../assets/thumbs/vt (18).jpg");
    });
});

