var util = require("./util.js");

var VideoSelector = function (videos, thumbnails, results) {
    this.set = 0;
    this.pos = 0;

    this.videos = videos;
    this.thumbnails = thumbnails;
    this.results = results;

    this.nextButton = document.getElementById("next");
    this.prevButton = document.getElementById("previous");

    this.videoPlayer = document.getElementById("vidbox");
    this.videoPlayer.autoplay = true;

    this.allPlayed = function () {
        for (var pos = 0; pos < util.numThumbnails; pos++) {
            var video = this.videos[util.getVideoID(this.set, pos)];
            if (video.hasPlayed == false) {
                return false;
            }
        }
        return true;
    };

    this.loadVideo = function () {
        var currentVideo = this.videos[util.getVideoID(this.set, this.pos)];
        this.videoPlayer.src = currentVideo.src;
    };

    this.loadThumbnails = function () {
        for (var pos = 0;pos < util.numThumbnails; pos++) {
            var video = this.videos[util.getVideoID(this.set, pos)];
            this.thumbnails[pos].setVideo(video);
        }
    };

    var self = this;
    this.nextClickHandler = function () {
        if (!self.allPlayed()) {
            console.log("Haven't played all videos!");
        } else if (self.results.getHappy() == null || self.results.getSad() == null) {
            console.log("Haven't got 1 happy and 1 sad video!");
        } else if (self.set < util.numSets-1) {
            self.set++;
            self.pos = 0;
            self.loadVideo();
            self.loadThumbnails();
        } else if (self.set == util.numSets-1) {
            console.log("Go to results page!");
            console.log(self.results.calculate());
        }
    };

    this.prevClickHandler = function () {
        if (self.set > 0) {
            self.set--;
            self.pos = 0;
            self.loadVideo();
            self.loadThumbnails();
        } else {
            console.log("Can't go back further!");
        }
    };

    this.endVideoHandler = function () {
        var currentVideo = self.videos[util.getVideoID(self.set, self.pos)];
        currentVideo.hasPlayed = true;
        currentVideo.thumbnail.drawImage();
    };

    this.nextButton.addEventListener("click", this.nextClickHandler);
    this.prevButton.addEventListener("click", this.prevClickHandler);
    this.videoPlayer.addEventListener("ended", this.endVideoHandler);
};

module.exports.VideoSelector = VideoSelector;
