var util = require("./util.js");

var VideoSelector = require("./videoSelector.js").VideoSelector;
var FaceSelector = require("./faceSelector.js").FaceSelector;
var Video = require("./video.js").Video;
var Thumbnail = require("./thumbnail.js").Thumbnail;
var Results = require("./results.js").Results;

if (window.attachEvent) {
    window.attachEvent('onload', start);
} else if(window.onload) {
    var currentOnload = window.onload;
    window.onload = function() {
        currentOnload();
        start();
    };
} else {
    window.onload = start;
}

function start () {
    // Build data structures
    var videos = [];
    var thumbnails = [];
    var results = new Results();
    var videoSelector = new VideoSelector(videos, thumbnails, results);
    var faceSelector = new FaceSelector(thumbnails);

    // Populate data structures
    for (var vidID = 0; vidID < util.numVideos; vidID++) {
        videos.push(new Video(vidID));
    }
    for (var pos = 0; pos < util.numThumbnails; pos++) {
        thumbnails.push(new Thumbnail(pos, videoSelector, faceSelector, results));
    }
    results.videoSelector = videoSelector;

    // Get first video and thumbnails
    videoSelector.loadVideo();
    videoSelector.loadThumbnails();
};
