var util = require("./util.js");

var Thumbnail = function (pos) {
    this.pos = pos;
    this.canvas = document.getElementById("thumb" + (pos+1) + "c");
    this.context = this.canvas.getContext("2d");

    this.videoSelector = null;
    this.faceSelector = null;
    this.results = null;
    this.video = null;

    var self = this;
    this.setVideo = function (video) {
        if (this.video != null) {
            this.video.thumbnail = null;
        }
        this.video = video;
        this.video.thumbnail = this;
        this.drawImage();
    };

    this.drawImage = function () {
        if (this.video.imageHasLoaded) {
            this.context.drawImage(this.video.image,0,0);
            if (this.results != null) {
                if (this.faceSelector.selectedFace != null &&
                    !this.video.hasPlayed) {
                    this.context.fillStyle = "rgba(128,128,128,0.5)"; // mid grey
                    this.context.fillRect(0,0,170,127);
                } else if (this.video.vidID == this.results.getHappy()) {
                    this.context.fillStyle = "rgba(0,255,20,0.3)"; // green
                    this.context.fillRect(0,0,170,127);
                } else if (this.video.vidID == this.results.getSad()) {
                    this.context.fillStyle = "rgba(255,0,0,0.3)"; // red
                    this.context.fillRect(0,0,170,127);
                }
            }
        }
    };

    var self = this;
    this.thumbnailClickHandler = function () {
        if (self.videoSelector == null || self.faceSelector == null ||
            self.results == null || self.video == null) {
            return;
        }

        // no face selected; choosing a video to play
        if (self.faceSelector.selectedFace == null) {
            self.videoSelector.pos = self.pos;
            self.videoSelector.loadVideo();
            return;
        }

        // liking / disliking a video that hasn't been played yet
        if (!self.video.hasPlayed) {
            console.log("You haven't watched that video!");
        // set happy video
        } else if ( self.faceSelector.selectedFace == "happy" &&
                    self.video.vidID != self.results.getSad()) {
            self.results.setHappy(self.pos);
        // set sad video
        } else if ( self.faceSelector.selectedFace == "sad" &&
                    self.video.vidID != self.results.getHappy()) {
            self.results.setSad(self.pos);
        }
        self.faceSelector.deselectFace();
    };

    this.canvas.addEventListener("click", function () {
        self.thumbnailClickHandler();
    }, false);
};

module.exports.Thumbnail = Thumbnail;
