(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var util = require("./util.js");

var FaceSelector = function (thumbnails) {
    this.happyButton = document.getElementById("happy");
    this.sadButton = document.getElementById("sad");
    this.selectedFace = null;
    this.thumbnails = thumbnails;

    this.redrawThumbnails = function () {
        for (var pos = 0; pos < util.numThumbnails; pos++) {
            this.thumbnails[pos].drawImage();
        }
    };

    this.selectHappy = function () {
        this.selectedFace = "happy";
        this.happyButton.style.outlineStyle = "solid";
        this.happyButton.style.outlineColor = "#00ff00";
        this.sadButton.style.outlineStyle = "none";
        this.redrawThumbnails();
    };

    this.selectSad = function () {
        this.selectedFace = "sad";
        this.sadButton.style.outlineStyle = "solid";
        this.sadButton.style.outlineColor = "#ff0000";
        this.happyButton.style.outlineStyle = "none";
        this.redrawThumbnails();
    };

    this.deselectFace = function () {
        this.selectedFace = null;
        this.happyButton.style.outlineStyle = "none";
        this.sadButton.style.outlineStyle = "none";
        this.redrawThumbnails();
    };

    var self = this;
    this.happyClickHandler = function () {
        if (self.selectedFace == "happy") {
            self.deselectFace();
        } else {
            self.selectHappy();
        }
    };

    this.sadClickHandler = function () {
        if (self.selectedFace == "sad") {
            self.deselectFace();
        } else {
            self.selectSad();
        }
    };

    this.happyButton.addEventListener("click", this.happyClickHandler);
    this.sadButton.addEventListener("click", this.sadClickHandler);
};

module.exports.FaceSelector = FaceSelector;

},{"./util.js":5}],2:[function(require,module,exports){
var util = require("./util.js");

var VideoSelector = require("./videoSelector.js").VideoSelector;
var FaceSelector = require("./faceSelector.js").FaceSelector;
var Video = require("./video.js").Video;
var Thumbnail = require("./thumbnail.js").Thumbnail;
var Results = require("./results.js").Results;

if (window.attachEvent) {
    window.attachEvent('onload', start);
} else if(window.onload) {
    var currentOnload = window.onload;
    window.onload = function() {
        currentOnload();
        start();
    };
} else {
    window.onload = start;
}

function start () {
    // Build data structures
    var videos = [];
    var thumbnails = [];
    var results = new Results();
    var videoSelector = new VideoSelector(videos, thumbnails, results);
    var faceSelector = new FaceSelector(thumbnails);

    // Populate data structures
    for (var vidID = 0; vidID < util.numVideos; vidID++) {
        videos.push(new Video(vidID));
    }
    for (var pos = 0; pos < util.numThumbnails; pos++) {
        thumbnails.push(new Thumbnail(pos, videoSelector, faceSelector, results));
    }
    results.videoSelector = videoSelector;

    // Get first video and thumbnails
    videoSelector.loadVideo();
    videoSelector.loadThumbnails();
};

},{"./faceSelector.js":1,"./results.js":3,"./thumbnail.js":4,"./util.js":5,"./video.js":6,"./videoSelector.js":7}],3:[function(require,module,exports){
var util = require("./util.js");

var Results = function () {
    this.happy = [];
    this.sad = [];
    for (var set = 0; set < util.numSets; set++) {
        this.happy.push(null);
        this.sad.push(null);
    }

    this.videoSelector = null;

    this.setHappy = function (pos) {
        if (this.videoSelector != null) {
            var set = this.videoSelector.set;
            this.happy[set] = util.getVideoID(set, pos);
        }
        return null;
    };

    this.setSad = function (pos) {
        if(this.videoSelector != null) {
            var set = this.videoSelector.set;
            this.sad[set] = util.getVideoID(set, pos);
        }
        return null;
    };

    this.getHappy = function () {
        if (this.videoSelector != null) {
            return this.happy[this.videoSelector.set];
        }
        return null;
    };

    this.getSad = function () {
        if (this.videoSelector != null) {
            return this.sad[this.videoSelector.set];
        }
        return null;
    };

    this.calculate = function () {
        var results = [];
        for (var vidID = 0; vidID < util.numVideos; vidID++) {
            results.push(0);
        }
        for (var set = 0; set < util.numSets; set++) {
            results[this.happy[set]] += 0.2;
            results[this.sad[set]] -= 0.2;
        }
        return results;
    };
};

module.exports.Results = Results;

},{"./util.js":5}],4:[function(require,module,exports){
var util = require("./util.js");

var Thumbnail = function (pos) {
    this.pos = pos;
    this.canvas = document.getElementById("thumb" + (pos+1) + "c");
    this.context = this.canvas.getContext("2d");

    this.videoSelector = null;
    this.faceSelector = null;
    this.results = null;
    this.video = null;

    var self = this;
    this.setVideo = function (video) {
        if (this.video != null) {
            this.video.thumbnail = null;
        }
        this.video = video;
        this.video.thumbnail = this;
        this.drawImage();
    };

    this.drawImage = function () {
        if (this.video.imageHasLoaded) {
            this.context.drawImage(this.video.image,0,0);
            if (this.results != null) {
                if (this.faceSelector.selectedFace != null &&
                    !this.video.hasPlayed) {
                    this.context.fillStyle = "rgba(128,128,128,0.5)"; // mid grey
                    this.context.fillRect(0,0,170,127);
                } else if (this.video.vidID == this.results.getHappy()) {
                    this.context.fillStyle = "rgba(0,255,20,0.3)"; // green
                    this.context.fillRect(0,0,170,127);
                } else if (this.video.vidID == this.results.getSad()) {
                    this.context.fillStyle = "rgba(255,0,0,0.3)"; // red
                    this.context.fillRect(0,0,170,127);
                }
            }
        }
    };

    var self = this;
    this.thumbnailClickHandler = function () {
        if (self.videoSelector == null || self.faceSelector == null ||
            self.results == null || self.video == null) {
            return;
        }

        // no face selected; choosing a video to play
        if (self.faceSelector.selectedFace == null) {
            self.videoSelector.pos = self.pos;
            self.videoSelector.loadVideo();
            return;
        }

        // liking / disliking a video that hasn't been played yet
        if (!self.video.hasPlayed) {
            console.log("You haven't watched that video!");
        // set happy video
        } else if ( self.faceSelector.selectedFace == "happy" &&
                    self.video.vidID != self.results.getSad()) {
            self.results.setHappy(self.pos);
        // set sad video
        } else if ( self.faceSelector.selectedFace == "sad" &&
                    self.video.vidID != self.results.getHappy()) {
            self.results.setSad(self.pos);
        }
        self.faceSelector.deselectFace();
    };

    this.canvas.addEventListener("click", function () {
        self.thumbnailClickHandler();
    }, false);
};

module.exports.Thumbnail = Thumbnail;

},{"./util.js":5}],5:[function(require,module,exports){
var videoOrder = [
    [18, 2, 6,14,10], [ 6, 1, 7, 8, 9], [16,11, 3, 6,21],
    [15, 6,20, 5,13], [19, 4,17,12, 6], [ 5, 3, 4, 2, 1],
    [ 2,19,15,11, 7], [13,17,21, 9, 2], [ 8,12, 2,20,16],
    [12,10, 1,13,11], [20, 7,10, 3,17], [10, 5, 9,16,19],
    [21,15, 8,10, 4], [ 1,21,19,18,20], [ 4,13,16, 7,18],
    [ 3, 9,18,15,12], [11,18, 5,17, 8], [17,16,14, 1,15],
    [ 7,14,12,21, 5], [ 9,20,11, 4,14], [14, 8,13,19, 3]
];

window.path = "../";

var getVideoID = function (set, pos) {
    return videoOrder[set][pos]-1;
};

var numVideos = 21;

var numSets = 21;

var numThumbnails = 5;

var getVideoPath = function (vidID) {
    return window.path+"assets/"+sessionStorage.lingo+"vid ("+(vidID+1)+").m4v";
};

var getThumbnailPath = function (vidID) {
    return window.path+"assets/"+sessionStorage.lingo+"thumbs/vt ("+(vidID+1)+").jpg";
};

module.exports.getVideoID = getVideoID;
module.exports.numVideos = numVideos;
module.exports.numSets = numSets;
module.exports.numThumbnails = numThumbnails;
module.exports.getVideoPath = getVideoPath;
module.exports.getThumbnailPath = getThumbnailPath;

},{}],6:[function(require,module,exports){
var util = require("./util.js");

var Video = function (vidID) {
    this.vidID = vidID;
    this.image = new Image();
    this.thumbnail = null;

    this.src = util.getVideoPath(vidID);
    this.image.src = util.getThumbnailPath(vidID);

    this.hasPlayed = false;
    this.imageHasLoaded = false;

    var self = this;
    this.image.onload = function () {
        self.imageHasLoaded = true;
        if (self.thumbnail != null) {
            self.thumbnail.drawImage();
        }
    };
};

module.exports.Video = Video;

},{"./util.js":5}],7:[function(require,module,exports){
var util = require("./util.js");

var VideoSelector = function (videos, thumbnails, results) {
    this.set = 0;
    this.pos = 0;

    this.videos = videos;
    this.thumbnails = thumbnails;
    this.results = results;

    this.nextButton = document.getElementById("next");
    this.prevButton = document.getElementById("previous");

    this.videoPlayer = document.getElementById("vidbox");
    this.videoPlayer.autoplay = true;

    this.allPlayed = function () {
        for (var pos = 0; pos < util.numThumbnails; pos++) {
            var video = this.videos[util.getVideoID(this.set, pos)];
            if (video.hasPlayed == false) {
                return false;
            }
        }
        return true;
    };

    this.loadVideo = function () {
        var currentVideo = this.videos[util.getVideoID(this.set, this.pos)];
        this.videoPlayer.src = currentVideo.src;
    };

    this.loadThumbnails = function () {
        for (var pos = 0;pos < util.numThumbnails; pos++) {
            var video = this.videos[util.getVideoID(this.set, pos)];
            this.thumbnails[pos].setVideo(video);
        }
    };

    var self = this;
    this.nextClickHandler = function () {
        if (!self.allPlayed()) {
            console.log("Haven't played all videos!");
        } else if (self.results.getHappy() == null || self.results.getSad() == null) {
            console.log("Haven't got 1 happy and 1 sad video!");
        } else if (self.set < util.numSets-1) {
            self.set++;
            self.pos = 0;
            self.loadVideo();
            self.loadThumbnails();
        } else if (self.set == util.numSets-1) {
            console.log("Go to results page!");
            console.log(self.results.calculate());
        }
    };

    this.prevClickHandler = function () {
        if (self.set > 0) {
            self.set--;
            self.pos = 0;
            self.loadVideo();
            self.loadThumbnails();
        } else {
            console.log("Can't go back further!");
        }
    };

    this.endVideoHandler = function () {
        var currentVideo = self.videos[util.getVideoID(self.set, self.pos)];
        currentVideo.hasPlayed = true;
        currentVideo.thumbnail.drawImage();
    };

    this.nextButton.addEventListener("click", this.nextClickHandler);
    this.prevButton.addEventListener("click", this.prevClickHandler);
    this.videoPlayer.addEventListener("ended", this.endVideoHandler);
};

module.exports.VideoSelector = VideoSelector;

},{"./util.js":5}]},{},[2]);
