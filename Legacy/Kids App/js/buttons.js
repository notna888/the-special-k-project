var NextButton = function (videoSelector, results) {
    this.nextButton = document.getElementById("next");
    this.videoSelector = videoSelector;
    this.results = results;

    var self = this;
    this.nextClickHandler = function () {
        var set = self.videoSelector.set;
        if (self.videoSelector.allPlayed() && self.results.happy[set] != null && self.results.sad[set] != null) {
            self.videoSelector.next();
        }
    };

    this.nextButton.addEventListener("click", this.nextClickHandler);
};

var PrevButton = function (videoSelector) {
    this.prevButton = document.getElementById("previous");
    this.videoSelector = videoSelector;

    var self = this;
    this.prevClickHandler = function () {
        self.videoSelector.prev();
    };

    this.prevButton.addEventListener("click", this.prevClickHandler);
};

module.exports.NextButton = NextButton;
module.exports.PrevButton = PrevButton;
