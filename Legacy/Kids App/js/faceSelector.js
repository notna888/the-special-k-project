var util = require("./util.js");

var FaceSelector = function (thumbnails) {
    this.happyButton = document.getElementById("happy");
    this.sadButton = document.getElementById("sad");
    this.selectedFace = null;
    this.thumbnails = thumbnails;

    this.redrawThumbnails = function () {
        for (var pos = 0; pos < util.numThumbnails; pos++) {
            this.thumbnails[pos].drawImage();
        }
    };

    this.selectHappy = function () {
        this.selectedFace = "happy";
        this.happyButton.style.outlineStyle = "solid";
        this.happyButton.style.outlineColor = "#00ff00";
        this.sadButton.style.outlineStyle = "none";
        this.redrawThumbnails();
    };

    this.selectSad = function () {
        this.selectedFace = "sad";
        this.sadButton.style.outlineStyle = "solid";
        this.sadButton.style.outlineColor = "#ff0000";
        this.happyButton.style.outlineStyle = "none";
        this.redrawThumbnails();
    };

    this.deselectFace = function () {
        this.selectedFace = null;
        this.happyButton.style.outlineStyle = "none";
        this.sadButton.style.outlineStyle = "none";
        this.redrawThumbnails();
    };

    var self = this;
    this.happyClickHandler = function () {
        if (self.selectedFace == "happy") {
            self.deselectFace();
        } else {
            self.selectHappy();
        }
    };

    this.sadClickHandler = function () {
        if (self.selectedFace == "sad") {
            self.deselectFace();
        } else {
            self.selectSad();
        }
    };

    this.happyButton.addEventListener("click", this.happyClickHandler);
    this.sadButton.addEventListener("click", this.sadClickHandler);
};

module.exports.FaceSelector = FaceSelector;
