var util = require("./util.js");

var Results = function () {
    this.happy = [];
    this.sad = [];
    for (var set = 0; set < util.numSets; set++) {
        this.happy.push(null);
        this.sad.push(null);
    }

    this.videoSelector = null;

    this.setHappy = function (pos) {
        if (this.videoSelector != null) {
            var set = this.videoSelector.set;
            this.happy[set] = util.getVideoID(set, pos);
        }
        return null;
    };

    this.setSad = function (pos) {
        if(this.videoSelector != null) {
            var set = this.videoSelector.set;
            this.sad[set] = util.getVideoID(set, pos);
        }
        return null;
    };

    this.getHappy = function () {
        if (this.videoSelector != null) {
            return this.happy[this.videoSelector.set];
        }
        return null;
    };

    this.getSad = function () {
        if (this.videoSelector != null) {
            return this.sad[this.videoSelector.set];
        }
        return null;
    };

    this.calculate = function () {
        var results = [];
        for (var vidID = 0; vidID < util.numVideos; vidID++) {
            results.push(0);
        }
        for (var set = 0; set < util.numSets; set++) {
            results[this.happy[set]] += 0.2;
            results[this.sad[set]] -= 0.2;
        }
        //sessionStorage.result = results;
        return results;
    };
};

module.exports.Results = Results;
