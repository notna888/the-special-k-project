var VideoPlayer = function (videoSelector) {
    this.video = document.getElementById("vidbox");
    this.videoSelector = videoSelector;

    var self = this;
    this.endVideoHandler = function () {
        self.videoSelector.getCurrentVideo().hasPlayed = true;
    };

    this.video.autoplay = true;
    this.video.addEventListener("ended", this.endVideoHandler);

    this.loadVideo = function () {
        var currentVideo = videoSelector.getCurrentVideo();
        this.video.src = currentVideo.src;
    };
};

module.exports.VideoPlayer = VideoPlayer;
