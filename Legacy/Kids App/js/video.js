var util = require("./util.js");

var Video = function (vidID) {
    this.vidID = vidID;
    this.image = new Image();
    this.thumbnail = null;

    this.src = util.getVideoPath(vidID);
    this.image.src = util.getThumbnailPath(vidID);

    this.hasPlayed = false;
    this.imageHasLoaded = false;

    var self = this;
    this.image.onload = function () {
        self.imageHasLoaded = true;
        if (self.thumbnail != null) {
            self.thumbnail.drawImage();
        }
    };
};

module.exports.Video = Video;
