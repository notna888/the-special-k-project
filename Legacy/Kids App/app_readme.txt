SO:
On my machine computer I have installed Node.js.
In Node.js I an using the npm (node packet manager, like downlaods special js files for use) for ease of use and readability.
Specifically i'm using the npm Browserify (which you can find by googling).
This combines multiple JS files into the one large file. Its pretty handy. 
You can install nodejs yourself off the website (for any os, i'm using windows here)
then in a console go "npm install browserify -g" or something along those lines (the website tells you the exact thing to run)
then when i want to test my program i type in the console 'browserify main.js -o complete.js'
which takes main, and all its dependencies (require('filename') at the top of main.js) and puts it all into one file for use in browsers.

Anyway you can start by reading through my js files and see what they do.
I'll try to comment as much as possible and make it make sense for you goize. 