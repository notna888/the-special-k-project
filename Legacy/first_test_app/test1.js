//testing for JS stuff
var test = 1;
var words = "hello";
var test2 = 5;
var test3 = test + test2;

console.log(test3);//6
console.log(words);//"hello"


//now lets try adding js objects:

var obj1 = function (i1,i2) {
    this.value1 = i1;
    this.value2 = i2;
    this.method1 = function (a) { return i2 + a};
};


var x = new obj1("word",1);
console.log(x.value1+" "+x.value2);//should print out "word 1"

//now lets try the method x has:
console.log(x.method1(2)); //should print out "3"

obj1.prototype.specialfunction = function (b) {
    //this gives the Super object a function (so shared by all instances of the object)
    //it saves on memory and runs a bit faster i think so is handy to use...
    this.value2 = this.value2 - b;
};
x.specialfunction(5);
console.log(x.value2);//should print "-4"


//now that thats over with, i'll throw some buttons in and attach them to some functions

var button = document.createElement("button");
var t = document.createTextNode("CLICK ME");       // Create a text node
button.appendChild(t);                                // Append the text to <button>
button.onclick = btnfunc;
document.body.appendChild(button);                    // Append <button> to <body>

function btnfunc()  {
    var c = document.createTextNode("WORDSZ");//add the word WORDZ to the html body
    document.body.appendChild(c);
}

console.log("bananas");