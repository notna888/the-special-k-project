var util = require("./util.js");

var Thumbnail = function (pos, videoSelector, faceSelector, results) {
    this.pos = pos;

    this.videoSelector = videoSelector;
    this.faceSelector = faceSelector;
    this.results = results;

    this.canvas = document.getElementById("thumb"+(pos+1)+"c");
    this.context = this.canvas.getContext("2d");
    this.video = null;

    this.setVideo = function (video) {
        if (this.video != null) {
            this.video.thumbnail = null;
        }
        this.video = video;
        this.video.thumbnail = this;
        this.drawImage();
    };

    this.drawImage = function () {
        if (this.video.imageHasLoaded) {
            this.context.drawImage(this.video.image,0,0,this.canvas.width,this.canvas.height);//must draw to the size of the canvas
            if (this.video.vidID == this.results.happy[this.videoSelector.set]) {
                this.fillHappy();
            }
            if (this.video.vidID == this.results.sad[this.videoSelector.set]) {
                this.fillSad();
            }
        }
    };

    this.fillHappy = function () {
        this.context.fillStyle="rgba(0,255,20,0.3)"; // green
        this.context.fillRect(0,0,170,127);
    };

    this.fillSad = function() {
        this.context.fillStyle="rgba(255,0,0,0.3)"; // red
        this.context.fillRect(0,0,170,127);
    };

    var self = this;
    this.thumbnailClickHandler = function () {
        var set = self.videoSelector.set;
        var vidID = util.getVideoID(set, pos);

        // set happy video
        if (self.faceSelector.selectedFace == "happy" && vidID != self.results.sad[set]) {
            self.faceSelector.deselectFace();
            var oldVidID = self.results.happy[set];

            // set new happy video
            self.results.setHappy(set, self.pos);
            self.fillHappy();

            // redraw previous happy canvas
            if (oldVidID != null) {
                videoSelector.videos[oldVidID].thumbnail.drawImage();
            }

        // set sad video
        } else if (self.faceSelector.selectedFace == "sad" && vidID != self.results.happy[set]) {
            self.faceSelector.deselectFace();
            var oldVidID = self.results.sad[set];

            // set new sad video
            self.results.setSad(set, self.pos);
            self.fillSad();

            // redraw previous sad canvas
            if (oldVidID != null) {
                videoSelector.videos[oldVidID].thumbnail.drawImage();
            }

        // no face selected
        } else if (self.faceSelector.selectedFace == null) {
            self.videoSelector.pos = self.pos;
            self.videoSelector.loadVideo();
        }
    };

    this.canvas.addEventListener("click", function () {
        self.thumbnailClickHandler();
    }, false);

};

module.exports.Thumbnail = Thumbnail;
