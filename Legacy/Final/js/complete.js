(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var NextButton = function (videoSelector, results) {
    this.nextButton = document.getElementById("next");
    this.videoSelector = videoSelector;
    this.results = results;

    var self = this;
    this.nextClickHandler = function () {
        var set = self.videoSelector.set;
        if (self.videoSelector.allPlayed() && self.results.happy[set] != null && self.results.sad[set] != null) {
            self.videoSelector.next();
        }
    };

    this.nextButton.addEventListener("click", this.nextClickHandler);
};

var PrevButton = function (videoSelector) {
    this.prevButton = document.getElementById("previous");
    this.videoSelector = videoSelector;

    var self = this;
    this.prevClickHandler = function () {
        self.videoSelector.prev();
    };

    this.prevButton.addEventListener("click", this.prevClickHandler);
};

module.exports.NextButton = NextButton;
module.exports.PrevButton = PrevButton;

},{}],2:[function(require,module,exports){
var FaceSelector = function (videoSelector) {
    this.happyButton = document.getElementById("happy");
    this.sadButton = document.getElementById("sad");
    this.selectedFace = null;
    this.videoSelector = videoSelector;

    this.highlightHappy = function () {
        this.happyButton.style.outlineStyle = "solid";
        this.happyButton.style.outlineColor = "#00ff00";
    };

    this.highlightSad = function () {
        this.sadButton.style.outlineStyle = "solid";
        this.sadButton.style.outlineColor = "#ff0000";
    };

    this.unHighlight = function () {
        this.happyButton.style.outlineStyle = "none";
        this.sadButton.style.outlineStyle = "none";
    };

    this.deselectFace = function () {
        this.selectedFace = null;
        this.unHighlight();
    };

    var self = this;
    this.happyClickHandler = function () {
        console.log("ITS WERKING");
        if (self.videoSelector.allPlayed()) {
            if (self.selectedFace == "happy") {
                self.deselectFace();
            } else {
                self.selectedFace = "happy";
                self.unHighlight();
                self.highlightHappy();
            }
        }
    };

    this.sadClickHandler = function () {
        if (self.videoSelector.allPlayed()) {
            if (self.selectedFace == "sad") {
                self.deselectFace();
            } else {
                self.selectedFace = "sad";
                self.unHighlight();
                self.highlightSad();
            }
        }
    };

    this.happyButton.addEventListener("click", this.happyClickHandler);
    this.sadButton.addEventListener("click", this.sadClickHandler);
};

module.exports.FaceSelector = FaceSelector;

},{}],3:[function(require,module,exports){
var util = require("./util.js");

var VideoSelector = require("./videoSelector.js");
var VideoSelector = VideoSelector.VideoSelector;

var FaceSelector = require("./faceSelector.js");
var FaceSelector = FaceSelector.FaceSelector;

var Results = require("./results.js");
var Results = Results.Results;

var Buttons = require("./buttons.js");
var NextButton = Buttons.NextButton;
var PrevButton = Buttons.PrevButton;

var VideoPlayer = require("./videoPlayer.js");
var VideoPlayer = VideoPlayer.VideoPlayer;

var Thumbnail = require("./thumbnail.js");
var Thumbnail = Thumbnail.Thumbnail;

if (window.attachEvent) {
    window.attachEvent('onload', start);
} else if(window.onload) {
    var currentOnload = window.onload;
    window.onload = function() {
        currentOnload();
        start();
    };
} else {
    window.onload = start;
}

function start () {
    var videoSelector = new VideoSelector();
    var faceSelector = new FaceSelector(videoSelector);
    var results = new Results(videoSelector);
    var nextButton = new NextButton(videoSelector, results);
    var prevButton = new PrevButton(videoSelector);

    var videoPlayer = new VideoPlayer(videoSelector);
    videoSelector.videoPlayer = videoPlayer;

    var thumbnails = [];
    for (var pos=0;pos<5;pos++) {
        thumbnails[pos] = new Thumbnail(pos, videoSelector, faceSelector, results);
    }
    videoSelector.thumbnails = thumbnails;

    videoSelector.loadThumbnails();
    videoPlayer.loadVideo();
};

},{"./buttons.js":1,"./faceSelector.js":2,"./results.js":4,"./thumbnail.js":5,"./util.js":6,"./videoPlayer.js":8,"./videoSelector.js":9}],4:[function(require,module,exports){
var util = require("./util.js");

var Results = function (videoSelector) {
    this.happy = [];
    this.sad = [];
    for (var set=0;set<21;set++) {
        this.happy.push(null);
        this.sad.push(null);
    }

    this.videoSelector = videoSelector;

    this.setHappy = function (set, pos) {
        if (videoSelector.set == set && videoSelector.allPlayed()) {
            this.happy[set] = util.getVideoID(set, pos);
        }
    };

    this.setSad = function (set, pos) {
        if (videoSelector.set == set && videoSelector.allPlayed()) {
            this.sad[set] = util.getVideoID(set, pos);
        }
    };

    this.calculate = function () {
        return false;
    };
};

module.exports.Results = Results;

},{"./util.js":6}],5:[function(require,module,exports){
var util = require("./util.js");

var Thumbnail = function (pos, videoSelector, faceSelector, results) {
    this.pos = pos;

    this.videoSelector = videoSelector;
    this.faceSelector = faceSelector;
    this.results = results;

    this.canvas = document.getElementById("thumb"+(pos+1)+"c");
    this.context = this.canvas.getContext("2d");
    this.video = null;

    this.setVideo = function (video) {
        if (this.video != null) {
            this.video.thumbnail = null;
        }
        this.video = video;
        this.video.thumbnail = this;
        this.drawImage();
    };

    this.drawImage = function () {
        if (this.video.imageHasLoaded) {
            this.context.drawImage(this.video.image,0,0);
            if (this.video.vidID == this.results.happy[this.videoSelector.set]) {
                this.fillHappy();
            }
            if (this.video.vidID == this.results.sad[this.videoSelector.set]) {
                this.fillSad();
            }
        }
    };

    this.fillHappy = function () {
        this.context.fillStyle="rgba(0,255,20,0.3)"; // green
        this.context.fillRect(0,0,170,127);
    };

    this.fillSad = function() {
        this.context.fillStyle="rgba(255,0,0,0.3)"; // red
        this.context.fillRect(0,0,170,127);
    };

    var self = this;
    this.thumbnailClickHandler = function () {
        var set = self.videoSelector.set;
        var vidID = util.getVideoID(set, pos);

        // set happy video
        if (self.faceSelector.selectedFace == "happy" && vidID != self.results.sad[set]) {
            self.faceSelector.deselectFace();
            var oldVidID = self.results.happy[set];

            // set new happy video
            self.results.setHappy(set, self.pos);
            self.fillHappy();

            // redraw previous happy canvas
            if (oldVidID != null) {
                videoSelector.videos[oldVidID].thumbnail.drawImage();
            }

        // set sad video
        } else if (self.faceSelector.selectedFace == "sad" && vidID != self.results.happy[set]) {
            self.faceSelector.deselectFace();
            var oldVidID = self.results.sad[set];

            // set new sad video
            self.results.setSad(set, self.pos);
            self.fillSad();

            // redraw previous sad canvas
            if (oldVidID != null) {
                videoSelector.videos[oldVidID].thumbnail.drawImage();
            }

        // no face selected
        } else if (self.faceSelector.selectedFace == null) {
            self.videoSelector.pos = self.pos;
            self.videoSelector.loadVideo();
        }
    };

    this.canvas.addEventListener("click", function () {
        self.thumbnailClickHandler();
    }, false);

};

module.exports.Thumbnail = Thumbnail;

},{"./util.js":6}],6:[function(require,module,exports){
var videoOrder = [
    [18, 2, 6,14,10], [ 6, 1, 7, 8, 9], [16,11, 3, 6,21],
    [15, 6,20, 5,13], [19, 4,17,12, 6], [ 5, 3, 4, 2, 1],
    [ 2,19,15,11, 7], [13,17,21, 9, 2], [ 8,12, 2,20,16],
    [12,10, 1,13,11], [20, 7,10, 3,17], [10, 5, 9,16,19],
    [21,15, 8,10, 4], [ 1,21,19,18,20], [ 4,13,16, 7,18],
    [ 3, 9,18,15,12], [11,18, 5,17, 8], [17,16,14, 1,15],
    [ 7,14,12,21, 5], [ 9,20,11, 4,14], [14, 8,13,19, 3]
];

var getVideoID = function (set, pos) {
    return videoOrder[set][pos]-1;
};

module.exports.getVideoID = getVideoID;

},{}],7:[function(require,module,exports){
var Video = function (vidID) {
    this.vidID = vidID;
    this.image = new Image();
    this.thumbnail = null;

    this.src = "./videos/vid (" + (vidID+1) + ").m4v";
    this.image.src = "./videos/thumbs/vt (" + (vidID+1) + ").jpg";

    this.hasPlayed = false;
    this.imageHasLoaded = false;

    var self = this;
    this.image.onload = function () {
        self.imageHasLoaded = true;
        if (self.thumbnail != null) {
            self.thumbnail.drawImage();
        }
    };
};

module.exports.Video = Video;

},{}],8:[function(require,module,exports){
var VideoPlayer = function (videoSelector) {
    this.video = document.getElementById("vidbox");
    this.videoSelector = videoSelector;

    var self = this;
    this.endVideoHandler = function () {
        self.videoSelector.getCurrentVideo().hasPlayed = true;
    };

    this.video.autoplay = true;
    this.video.addEventListener("ended", this.endVideoHandler);

    this.loadVideo = function () {
        var currentVideo = videoSelector.getCurrentVideo();
        this.video.src = currentVideo.src;
    };
};

module.exports.VideoPlayer = VideoPlayer;

},{}],9:[function(require,module,exports){
var util = require("./util.js");

var Video = require("./video.js");
var Video = Video.Video;

var VideoSelector = function () {
    this.set = 0;
    this.pos = 0;

    this.videoPlayer = null;
    this.thumbnails = null;

    this.videos = [];
    for (var vidID=0;vidID<21;vidID++) {
        this.videos.push(new Video(vidID));
    }

    this.getCurrentVideo = function () {
        return this.videos[util.getVideoID(this.set, this.pos)];
    };

    this.allPlayed = function () {
        for (var pos=0;pos<5;pos++) {
            var video = this.videos[util.getVideoID(this.set, pos)];
            if (video.hasPlayed == false) {
                return false;
            }
        }
        return true;
    };

    this.loadThumbnails = function () {
        if (this.thumbnails != null) {
            for (var pos=0;pos<5;pos++) {
                var video = this.videos[util.getVideoID(this.set, pos)];
                this.thumbnails[pos].setVideo(video);
            }
        }
    };

    this.loadVideo = function () {
        if (this.videoPlayer != null) {
            this.videoPlayer.loadVideo();
        }
    };

    this.next = function () {
        if (this.set < 20) {
            this.set++;
            this.pos = 0;
            this.loadThumbnails();
            this.loadVideo();
        }
    };

    this.prev = function () {
        if (this.set > 0) {
            this.set--;
            this.pos = 0;
            this.loadThumbnails();
            this.loadVideo();
        }
    };
};

module.exports.VideoSelector = VideoSelector;

},{"./util.js":6,"./video.js":7}]},{},[3]);
