var util = require("./util.js");

var Results = function (videoSelector) {
    this.happy = [];
    this.sad = [];
    for (var set=0;set<21;set++) {
        this.happy.push(null);
        this.sad.push(null);
    }

    this.videoSelector = videoSelector;

    this.setHappy = function (set, pos) {
        if (videoSelector.set == set && videoSelector.allPlayed()) {
            this.happy[set] = util.getVideoID(set, pos);
        }
    };

    this.setSad = function (set, pos) {
        if (videoSelector.set == set && videoSelector.allPlayed()) {
            this.sad[set] = util.getVideoID(set, pos);
        }
    };

    this.calculate = function () {
        return false;
    };
};

module.exports.Results = Results;
