var util = require("./util.js");

var Video = require("./video.js");
var Video = Video.Video;

var VideoSelector = function () {
    this.set = 0;
    this.pos = 0;

    this.videoPlayer = null;
    this.thumbnails = null;

    this.videos = [];
    for (var vidID=0;vidID<21;vidID++) {
        this.videos.push(new Video(vidID));
    }

    this.getCurrentVideo = function () {
        return this.videos[util.getVideoID(this.set, this.pos)];
    };

    this.allPlayed = function () {
        for (var pos=0;pos<5;pos++) {
            var video = this.videos[util.getVideoID(this.set, pos)];
            if (video.hasPlayed == false) {
                return false;
            }
        }
        return true;
    };

    this.loadThumbnails = function () {
        if (this.thumbnails != null) {
            for (var pos=0;pos<5;pos++) {
                var video = this.videos[util.getVideoID(this.set, pos)];
                this.thumbnails[pos].setVideo(video);
            }
        }
    };

    this.loadVideo = function () {
        if (this.videoPlayer != null) {
            this.videoPlayer.loadVideo();
        }
    };

    this.next = function () {
        if (this.set < 20) {
            this.set++;
            this.pos = 0;
            this.loadThumbnails();
            this.loadVideo();
        }
    };

    this.prev = function () {
        if (this.set > 0) {
            this.set--;
            this.pos = 0;
            this.loadThumbnails();
            this.loadVideo();
        }
    };
};

module.exports.VideoSelector = VideoSelector;
