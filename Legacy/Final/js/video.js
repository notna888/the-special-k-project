var Video = function (vidID) {
    this.vidID = vidID;
    this.image = new Image();
    this.thumbnail = null;

    this.src = "../assets/vid (" + (vidID+1) + ").m4v";
    this.image.src = "../assets/thumbs/vt (" + (vidID+1) + ").jpg";

    this.hasPlayed = false;
    this.imageHasLoaded = false;

    var self = this;
    this.image.onload = function () {
        self.imageHasLoaded = true;
        if (self.thumbnail != null) {
            self.thumbnail.drawImage();
        }
    };
};

module.exports.Video = Video;
