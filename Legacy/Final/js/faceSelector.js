var FaceSelector = function (videoSelector) {
    this.happyButton = document.getElementById("happy");
    this.sadButton = document.getElementById("sad");
    this.selectedFace = null;
    this.videoSelector = videoSelector;

    this.highlightHappy = function () {
        this.happyButton.style.outlineStyle = "solid";
        this.happyButton.style.outlineColor = "#00ff00";
    };

    this.highlightSad = function () {
        this.sadButton.style.outlineStyle = "solid";
        this.sadButton.style.outlineColor = "#ff0000";
    };

    this.unHighlight = function () {
        this.happyButton.style.outlineStyle = "none";
        this.sadButton.style.outlineStyle = "none";
    };

    this.deselectFace = function () {
        this.selectedFace = null;
        this.unHighlight();
    };

    var self = this;
    this.happyClickHandler = function () {
        if (self.videoSelector.allPlayed()) {
            if (self.selectedFace == "happy") {
                self.deselectFace();
            } else {
                self.selectedFace = "happy";
                self.unHighlight();
                self.highlightHappy();
            }
        }
    };

    this.sadClickHandler = function () {
        if (self.videoSelector.allPlayed()) {
            if (self.selectedFace == "sad") {
                self.deselectFace();
            } else {
                self.selectedFace = "sad";
                self.unHighlight();
                self.highlightSad();
            }
        }
    };

    this.happyButton.addEventListener("click", this.happyClickHandler);
    this.sadButton.addEventListener("click", this.sadClickHandler);
};

module.exports.FaceSelector = FaceSelector;
