var util = require("./util.js");

var VideoSelector = require("./videoSelector.js");
var VideoSelector = VideoSelector.VideoSelector;

var FaceSelector = require("./faceSelector.js");
var FaceSelector = FaceSelector.FaceSelector;

var Results = require("./results.js");
var Results = Results.Results;

var Buttons = require("./buttons.js");
var NextButton = Buttons.NextButton;
var PrevButton = Buttons.PrevButton;

var VideoPlayer = require("./videoPlayer.js");
var VideoPlayer = VideoPlayer.VideoPlayer;

var Thumbnail = require("./thumbnail.js");
var Thumbnail = Thumbnail.Thumbnail;

if (window.attachEvent) {
    window.attachEvent('onload', start);
} else if(window.onload) {
    var currentOnload = window.onload;
    window.onload = function() {
        currentOnload();
        start();
    };
} else {
    window.onload = start;
}

function start () {
    var videoSelector = new VideoSelector();
    var faceSelector = new FaceSelector(videoSelector);
    var results = new Results(videoSelector);
    var nextButton = new NextButton(videoSelector, results);
    var prevButton = new PrevButton(videoSelector);

    var videoPlayer = new VideoPlayer(videoSelector);
    videoSelector.videoPlayer = videoPlayer;

    var thumbnails = [];
    for (var pos=0;pos<5;pos++) {
        thumbnails[pos] = new Thumbnail(pos, videoSelector, faceSelector, results);
    }
    videoSelector.thumbnails = thumbnails;

    videoSelector.loadThumbnails();
    videoPlayer.loadVideo();
};
